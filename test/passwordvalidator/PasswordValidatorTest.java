/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordvalidator;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class PasswordValidatorTest {
    
    public PasswordValidatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testValidatePasswordLengthRegular() {
        System.out.println("validate Password length Regular");
        String password = "abcdefghijk";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = true;
        boolean result = instance.validateLength(password);
        assertEquals("The length of the password meets the requirement" , expResult, result);
    }
   
        @Test
    public void testValidatePasswordLengthException() {
        System.out.println("validate Password length Exception");
        String password = "";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validateLength(password);
        assertEquals("The length of the password does not meet the requirement" , expResult, result);
    }
    
          @Test
    public void testValidatePasswordLengthBoundaryIn() {
        System.out.println("validatePassword length Boundary in");
        String password = "abcdefgh";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = true;
        boolean result = instance.validateLength(password);
        assertEquals("The length of the password meets the requirement" , expResult, result);
    }
    
          @Test
    public void testValidatePasswordLengthBoundaryOut() {
        System.out.println("validate Password length boundary out");
        String password = "abcdefg";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validateLength(password);
        assertEquals("The length of the password does not meet the requirement" , expResult, result);
    }

      @Test
    public void testValidatePasswordSpecCharRegualr() {
        System.out.println("validate Password contains one special character Regular");
        String password = "!*@";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = true;
        boolean result = instance.validateSpecChar(password);
        assertEquals("The password meets the requirement" , expResult, result);
    }
   
        @Test
    public void testValidatePasswordSpecCharException() {
        System.out.println("validate Password contains one special character Exception");
        String password = "";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validateSpecChar(password);
        assertEquals("The password does not meet the requirement" , expResult, result);
    }
    
          @Test
    public void testValidatePasswordSpecCharBoundaryIn() {
        System.out.println("validate Password contains one special character Boundary in");
        String password = "!";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = true;
        boolean result = instance.validateSpecChar(password);
        assertEquals("The password meets the requirement", expResult, result);
    }
    
          @Test
    public void testValidatePasswordSpecCharBoundaryOut() {
        System.out.println("validate Password contains one special character Boundary out");
        String password = "a";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validateSpecChar(password);
        assertEquals("The password does not meet the requirement" , expResult, result);
    }
    
      @Test
    public void testValidatePasswordDigitRegular() {
        System.out.println("validate Password contains one digit Regular");
        String password = "9999";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = true;
        boolean result = instance.validateDigit(password);
        assertEquals("The password meets the requirement", expResult, result);
    }
   
        @Test
    public void testValidatePasswordDigitException() {
        System.out.println("validate Password contains one digit Exception");
        String password = "";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validateDigit(password);
        assertEquals("The password does not meet the requirement" , expResult, result);
    }
    
          @Test
    public void testValidatePasswordDigitBoundaryIn() {
        System.out.println("validate Password contains one digit Boundary in");
        String password = "9";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = true;
        boolean result = instance.validateDigit(password);
        assertEquals("The password meets the requirement", expResult, result);
    }
    
          @Test
    public void testValidatePasswordDigitBoundaryOut() {
        System.out.println("validate Password contains one digit Boundary out");
        String password = "a";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validateDigit(password);
        assertEquals("The password does not meet the requirement" , expResult, result);
    }
    
      @Test
    public void testValidatePasswordUpperCaseRegular() {
        System.out.println("validate Password length regular");
        String password = "AMAZING";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = true;
        boolean result = instance.validateUpperCase(password);
        assertEquals("The password meets the requirement", expResult, result);
    }
   
        @Test
    public void testValidatePasswordUpperCaseException() {
        System.out.println("validate Password length exception");
        String password = "";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validateUpperCase(password);
        assertEquals("The password does not meet the requirement" , expResult, result);
    }
    
          @Test
    public void testValidatePasswordUpperCaseBoundaryIn() {
        System.out.println("validatePassword length boundary in");
        String password = "Amazing";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = true;
        boolean result = instance.validateUpperCase(password);
        assertEquals("The password meets the requirement", expResult, result);
    }
    
          @Test
    public void testValidatePasswordUpperCaseBoundaryOut() {
        System.out.println("validate Password length boundary out");
        String password = "amazing";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validateUpperCase(password);
        assertEquals("The password does not meet the requirement" , expResult, result);
    }

    
}
