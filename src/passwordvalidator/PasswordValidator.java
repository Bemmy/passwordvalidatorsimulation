/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordvalidator;

public class PasswordValidator {

 public PasswordValidator(){}
 
 public boolean  validatePassword(String password){
     
        boolean valid = false;
        int criteriaCount = 0;
                
        if(validateLength(password) == true) criteriaCount++; 
        if(validateDigit(password) == true) criteriaCount++; 
        if(validateSpecChar(password) == true) criteriaCount++; 
        if(validateUpperCase(password) == true) criteriaCount++; 
        
        if(criteriaCount == 4){
            
            valid = true;
            System.out.println("This password is valid and meets all 4 of the required criteria");
            
        } else {
            
        System.out.println("This password is not valid because it  meets " + criteriaCount + " of the 4 "
                + "criteria required for a valid password.");
        }
     
        return valid;
 }
 
 protected static boolean validateLength(String password){
            
           boolean valid = false;
           
           if (password.length() >= 8) valid = true;
           
           return valid;
 }
 
  protected static boolean validateSpecChar(String password){
            
           boolean valid = false;
           
           for(int i = 0; i < password.length(); i++){
               
           if(password.substring(i, i + 1).matches("[^A-Za-z0-9]+")) valid = true;
           
           }
              return valid;
 }
  
   protected static boolean validateDigit(String password){
         
        boolean valid = false;
        
        for(int i = 0; i < password.length(); i++){
            char ch = password.charAt(i);
            
        if(Character.isDigit(ch)) valid = true;
        }
           return valid;
 }
  
   protected static boolean validateUpperCase(String password){
         
        boolean valid = false;
        
          for(int i = 0; i < password.length(); i++){
               
           if(password.substring(i, i + 1).matches("[A-Z]+")) valid = true;
           
           }
          
            return valid;
   }   
}

