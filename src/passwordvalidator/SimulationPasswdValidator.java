/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordvalidator;

public class SimulationPasswdValidator {
    
    public static void main(String[] args){
        
        boolean valid = false;
        String password = "Mypasswordis9!";        
        
        PasswordValidator validator = new PasswordValidator();

        valid = validator.validatePassword(password);
        
        System.out.println("Password Validator returns: " + valid);
    }
}
